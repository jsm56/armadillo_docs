.. armadillo documentation master file, created by
   sphinx-quickstart on Wed May 20 06:19:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. figure:: logo.png
    :align: center


ARMADiLLO stands for "Antigen Receptor Mutation Analyzer for Detection of Low-Likelihood Occurrences" and is used for calculating the probability of amino acid mutations of antibody sequence. ARMADiLLO identifies improbable antibody mutations that are no routinely targeted by the somatic hypermutation machinery during B cell maturation.

The ARMADiLLO website can be found at:

`Online Website <https://armadillo.dhvi.duke.edu/>`_


Features
--------


.. image:: _static/armadillo_abstract_graph.jpg
	    :align: center
	    :scale: 50


- The singularity container takes antibody sequences and calculates the UCA
- The UCA sequences are matured in ARMADiLLO by subjecting them to several rounds of mutation
- The resulting mutated sequences are then used to calculate the probability of each mutation in the antibody
   
  
Table of Context
----------------

.. toctree::
	     
   science
   installation
   inputs
   output
   trouble
	      


---------

`Wiehe K., Bradley T., Meyerhoff R.R., Hart C. Williams W.B., Easterhoff D., Faison W.J., Kepler T.B., Saunders K.O., Alam S.M., Bonsignori M. and Haynes B.F. (2018) Functional Relevance of Improbable Antibody Mutations for HIV Broadly Neutralizing Antibody Development. Cell Host & Microbe. 23(6):759-765. <https://pubmed.ncbi.nlm.nih.gov/29861171/>`_

