================
Troubleshooting
================

Installation
============

Cannot find Boost Libraries
---------------------------

If the boost libraries can not be found, make sure they are installed on the system. The boost libraries for C++ can be found at https://www.boost.org/. Once the libraries are confirmed to be installed, update the Makefile lines 13 & 14 (for linux, lines 10 & 11 for OSX) to point at the boost libraries. Rerun the makefile to compile.


Container is not Recognized
---------------------------

The first step is to confirm the installation of Singularity. Please see the Singularity guide at : 'Singularity guide <https://sylabs.io/guides/3.5/admin-guide/installation.html>'_. If singularity is installed but you lack access, please contact your system admin for access.


Running
=======

Using a user generated UCA
--------------------------

If you have generated a UCA sequence through other methods, the UCA sequence can be directly used in ARMADiLLO to calculate the probability of mutations.

To run ARMADiLLO using a pre-generated UCA (uca.seq.fasta) with an antibody sequence (antibody.seq.fasta) use the command::

    singularity run --app armadillo armadillo.simg -uca uca.seq.fasta -seq antibody.seq.fasta

No output generated
-------------------

This issue is usually the result of not being able to interact with the directory containing the data. This is necessary to write the output files to the directory. To fix this use the bind argument pointing to the path were the data is::

 singularity run --bind /path/to/data  armadillo.simg -f antibody.seq.fasta

Cannot find sequence file
-------------------------

This issue is identified when the container returns the error

 no fasta file for processing found:  /path/to/antibody.fasta
 
The solution is two fold; first run the container within the directory containing the antibody sequence and second to bind the path to the antibody file::

 singularity run --bind /path/to/data  armadillo.simg -f antibody.seq.fasta
 
