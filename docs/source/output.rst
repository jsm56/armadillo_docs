================
ARMADiLLO Output
================

ARMADiLLO produces multiple outputs including a list of number of mutations which is printed to the standard out. Each unique name of the antibodies analyzed produces a set of files. The set of files depends on which flag is used to indicate output. The list of flags with example files for the CH01 bNAB are shown\:

  -	 -simple_text
	 
	 - CH01.ARMADiLLO.fasta
	 
  -	 -text
	 
	 - CH01.ARMADiLLO.fasta

	 - CH01.ARMADiLLO.Detailed.text

	 - CH01.freq_table.txt
	 
  -	 -HTML
	 
	 - CH01.ARMADiLLO.html
	    
	 - CH01.freq_table.html

	 - CH01.tiles.html
	 
  -	 -fulloutput
	 
	 - CH01.ARMADiLLO.fasta

	 - CH01.ARMADiLLO.Detailed.text

	 - CH01.freq_table.txt

	 - CH01.ARMADiLLO.html
	    
	 - CH01.freq_table.html

	 - CH01.tiles.html

	 

Print to Screen
---------------

ARMADiLLO prints to standard out when run. A screen shot of the output is shown below.

.. figure:: _static/stdout.armadillo.png
    :align: center

The column headers are labeled. The important columns are the first column containing the antibody label and the last column *sum(log(P))*. This output can be directed to a file (here called results.txt) using the command::

   ARMADiLLO -m Mutability.csv -s Substitution.csv <arguments> > results.txt
   
	    

Results Files
-------------

CH01.ARMADiLLO.html
 The Details tab displays a nucleotide level view of the input sequence aligned to the inferred UCA. The first three rows of each block comprises the UCA sequence. The next 3 rows comprise the input sequence and the last row is the probability of the amino acid observed at that position in the input sequence. The first row is the UCA amino acid sequence. The second row is the amino acid position number (using a simple numbering scheme starting at 1 at the first residue). The third row is the nucleotide sequence. Below each base in this row is the mutability score calculated using the S5F model. AID hot spots are highlighted in red (mutability score >2) and AID cold spots are highlighted in blue (mutability score <0.3). The fourth row is the amino acid sequence of the input sequence. Mutations are highlighted in yellow. The fifth row is the amino acid position number of the input sequence. The sixth row is the nucleotide sequence of the input sequence with the mutability score listed below and hot spots and cold spots highlighted as in the third row. The seventh row is the estimated probability of the amino acid observed in the input sequence. Hovering over the probability in the 7th row will display the probabilities of all 20 amino acids for the position. A red arrow below the alignment displays any nucleotide changes that are the result of a mutation at an AID cold spot. 
 
.. figure:: _static/CH01.table.png
    :align: center
 
CH01.tiles.html
 The Overview tab displays an amino acid alignment of the sequence of interest aligned to the inferred UCA (inferred automatically by the program Cloanalyst ). Mutations are colored by their probability according to the legend below the alignment. A PDF or image of this view can be downloaded by clicking on the "Download PDF/PNG" icons at the bottom of the page. 
 
.. figure:: _static/CH01.screenshot.png
    :align: center
   
CH01.freq_table.html
 partial frequency table
 
.. figure:: _static/CH01.freqTable.png
    :align: center
	    

CH01.ARMADiLLO.fasta
 This is a text version of the tiles HTML file. The text file is similar to a fasta file containing the first line is the antibody name, second name is the UCA, the third is the antibody sequence, and the fourth is indicating mutations. The categories of probabilities identified by the number with the probability range corresponding to the number shown in the table below::
   
   >CH01
   EVQLVESGGGVVRPGGSLRLSCAASGFTFDDYGMSWVRQAPGKGLEWVSGINWNGGSTGYADSVKGRFTISRDNAKNSLYLQMNSLRAEDTALYHCARGTDYTIDDAGIHYYGSGTYWYFDLWGRGTLVTVSS
   EVQLVESGANVVRPGGSLRLSCKASGFIFENFGFSWVRQAPGKGLQWVAGLNWNGGDTRYADSVKGRFRMSRDNSRNFVYLDMDKVGVDDTAFYYCARGTDYTIDDAGIHYQGSGTFWYFDLWGRGTLVSVSS
   --------24------------4----2-221-4-----------3--3-1-----3-2---------42----32-23--4-253212---1-2----------------4----2------------2---

 Table below shows the values found in the ARMADiLLO.fasta file which correspond to probability ranges:
 
.. list-table::
   :widths: 5 10 5 5 5 5 5 5
   :header-rows: 0
   :align: center

   * - X
     - \-
     - 1
     - 2
     - 3
     - 4
     - 5
     - 6
   * - indel
     - no mutation
     - >10%
     - 10%-2%
     - 2%-1%
     - 1%-0.1%
     - 0.1%-0.01%
     - <0.01%

CH01.ARMADiLLO.Detailed.txt
 Detailed text file containing information similar to the data found in the <name>.ARMADiLLO.html file. Below is a portion of the CH01.ARMADiLLO.Detailed.txt::
 
   sequence name:	CH01
   markup header:	CH01|IGHV3-20*01|IGHD3-10*01|IGHJ2*01

   pos	UCA AA	UCA NT	seq AA	seq NT		P(NT)	P(AA)
   0	E	G	E	G		N/A	0.693
   1		A		A		N/A	 
   2		G		G		0.993	 
   3	V	G	V	G		1.869	0.740
   4		T		T		1.064	 
   5		G		T	*	0.320	 
   6	Q	C	Q	C		0.792	0.750
   7		A		A		1.049	 
   8		G		G		2.214	 
   9	L	C	L	C		3.089	0.741
   10		T		T		0.308	 
   11		G		G		0.304	 
   12	V	G	V	G		0.987	0.725
   13		T		T		0.784	 
   14		G		G		0.599	 
   15	E	G	E	G		0.406	0.777
   16		A		A		0.891	 
   17		G		G		0.456	 
   18	S	T	S	T		0.353	0.894
   19		C		C		0.216	 
   20		T		T		0.475	 
   21	G	G	G	G		0.180	0.937

CH01.freq_table.txt
 This text file containing the frequency table that is identical to the <name>.freq_table.html file. Below is a portion of the CH01.freq_table.txt file as an example::

   pos,A,C,D,E,F,G,H,I,K,L,M,N,P,Q,R,S,T,V,W,Y
   1,0.046026,0.000109,0.054888,0.692587,0.000102,0.046116,0.002774,0.000376,0.04222,0.004152,0.002404,0.003095,0.002883,0.043106,0.005473,0.001668,0.00289,0.045995,0.001375,0.001761
   2,0.039752,0.000105,0.001162,0.029505,0.002204,0.026113,5.7e-05,0.019598,0.003511,0.068141,0.05775,0.000223,0.001677,0.000902,0.002998,0.001303,0.005039,0.739662,0.000286,1.2e-05
   3,0.00052,0.000295,0.001778,0.009705,0.00023,0.000777,0.100629,0.000558,0.043009,0.023889,0.000785,0.007063,0.017885,0.749523,0.036306,0.001211,0.00173,0.000734,0.002155,0.001218
   4,0.005967,7.9e-05,0.000128,0.003383,0.004667,0.002746,0.000281,0.005609,0.003495,0.74079,0.054421,0.000138,0.022387,0.007966,0.010551,0.007812,0.004093,0.123653,0.00181,2.4e-05
   5,0.049846,7e-06,0.001734,0.034422,0.000482,0.029925,4.8e-05,0.010574,0.00287,0.068665,0.064194,8.5e-05,0.002003,0.000783,0.002501,0.001101,0.005371,0.725208,0.000172,9e-06
   6,0.045533,1.3e-05,0.028818,0.777286,1.8e-05,0.064965,0.001012,0.000129,0.035438,0.001617,0.001855,0.001247,0.001431,0.014608,0.002706,0.000526,0.003117,0.019375,0.000168,0.000138
   7,0.015214,0.016725,0.000158,1.1e-05,0.0169,0.000367,0.000314,0.000354,5e-06,0.001932,1.5e-05,0.000234,0.034619,1.3e-05,0.000611,0.894044,0.012548,0.00041,0.000418,0.005108
   8,0.004763,9e-05,0.000849,0.020644,4e-06,0.937063,1.6e-05,2.4e-05,0.000569,0.000323,0.000183,2.1e-05,0.000151,0.00037,0.022485,0.000273,0.00034,0.005244,0.006577,1.1e-05
   9,0.023785,0.000111,0.00259,0.067305,2.5e-05,0.684705,0.000152,0.002306,0.005354,0.001983,0.000159,0.000294,0.001659,0.001073,0.170454,0.021486,0.008058,0.008393,8.7e-05,2.1e-05
   10,0.033872,0.026353,0.066866,0.009225,0.00099,0.708193,0.003698,0.002701,0.000438,0.001442,3.6e-05,0.006936,0.005062,0.000218,0.034596,0.069495,0.008695,0.019157,0.000308,0.001719
   11,0.036335,3.9e-05,0.001563,0.027254,0.001818,0.024736,5.2e-05,0.007692,0.001469,0.096155,0.031858,9e-05,0.001998,0.00097,0.002466,0.001758,0.003098,0.759726,0.000903,2e-05
   12,0.06637,7.4e-05,0.008767,0.105961,0.004292,0.066928,0.000354,0.123754,0.014916,0.138731,0.012939,0.001347,0.01009,0.004029,0.015095,0.005556,0.020502,0.400189,5.7e-05,4.9e-05
   13,0.000327,0.003646,3.4e-05,0.000743,4.3e-05,0.051235,0.00096,1.9e-05,0.000499,0.004769,0.000317,1.7e-05,0.00611,0.010093,0.807677,0.0019,0.000338,0.000336,0.110877,6e-05
   14,0.007415,0.000255,5.7e-05,2e-05,0.000271,0.000107,0.006102,0.000187,8e-06,0.016516,8e-06,0.000103,0.933412,0.000631,0.013233,0.01211,0.009221,0.000264,1.8e-05,6.2e-05
   15,0.003675,6.8e-05,0.000483,0.016696,4e-06,0.944095,1.1e-05,1.3e-05,0.000409,0.000251,0.000179,1.5e-05,0.000112,0.000308,0.022632,0.000242,0.000256,0.004183,0.006364,4e-06
   16,0.012839,0.000121,0.002129,0.049045,5e-06,0.888193,6.8e-05,0.000101,0.002773,0.000612,0.000711,0.000136,0.000588,0.001367,0.022944,0.000618,0.00105,0.012591,0.004096,1.3e-05
   17,0.017927,0.021411,0.000689,2.4e-05,0.01949,0.000487,0.000889,0.000489,6.6e-05,0.003052,2.6e-05,0.001049,0.041783,1.9e-05,0.000626,0.871477,0.008914,0.000662,0.000517,0.010403
   18,0.000421,1.3e-05,1.3e-05,0.000281,0.001165,0.000219,0.000646,0.000517,0.000219,0.909542,0.006267,5e-06,0.033152,0.017986,0.015097,0.000559,0.00047,0.013217,0.000205,6e-06
   19,0.00052,0.000143,8.7e-05,0.001066,1.2e-05,0.042885,4.6e-05,0.004404,0.014997,0.000218,0.000382,0.001109,0.000395,0.00044,0.90871,0.013309,0.010852,0.00024,0.000182,3e-06


HTML Style Files
----------------

These style files are required for proper viewing of the HTML files in a web browser. ARMADiLLO will automatically create these files in the same directory as the HTML files. If the file HTML files are moved then these style files should be copied as well.

* AMA.css

* sequence_color.css



	    


	    
