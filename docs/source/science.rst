=======
Science
=======

Not all mutations during B cell affinity maturation are equally probable. We have shown that HIV-1 broadly neutralizing antibodies (bnAbs) are enriched with low probability mutations and that these improbable mutations are often critical for HIV-1 bnAb neutralization breadth, thus making improbable mutations key targets for selection with vaccines (`Wiehe et. al 2018 <https://pubmed.ncbi.nlm.nih.gov/29861171/>`_\).

.. figure:: _static/armadillo_abstract_graph.jpg
	    :align: center
            :scale: 50



Antibody Maturation
--------------------

Excerpted from `Wiehe et. al 2018 <https://pubmed.ncbi.nlm.nih.gov/29861171/>`_\:

Somatic hypermutation occurs prior to antigen affinity-based selection during affinity maturation (De Silva and Klein, 2015; Victora and Nussenzweig, 2012). Somatic hypermutation is mediated by activation-induced cytidine deaminase (AID) (Di Noia and Neuberger, 2007), and AID preferentially targets specific nucleotide sequence motifs (‘‘hot spots’’), whereas targeting of other nucleotide motifs (‘‘cold spots’’) is disfavored (Betz et al., 1993; Pham et al., 2003; Yaari et al., 2013). AID initiates DNA lesions, and their subsequent repair results in a bias for certain bases to be substituted at the targeted position (Cowell and Kepler, 2000). The consequence of this non-uniformly random mutation process is that specific amino acid substitutions occur with varying frequencies prior to antigenic selection. Mutations at AID hotspots can occur frequently in the absence of antigen selection due to immune-activation-associated AID activity (Bonsignori et al., 2016; Yeap et al., 2015). Amino acid substitutions that occur infrequently generally require strong antigenic selection in order to arise during maturation (Brown et al., 1992; Kocks and Rajewsky, 1988). Such rare amino acid substitutions are improbable prior to selection for two reasons: 1) base mutations must occur at AID cold spots, and 2) due to codon mapping, multiple base substitutions must occur for a specific amino acid change. Within the critical subset of mutations that grant broad neutralization capacity to a bnAb lineage, those key mutations that are also improbable prior to selection may represent important events in bnAb maturation. ARMADiLLO is designed to answer these questions.



Unmutated Common Ancestor Identification
----------------------------------------

To perform analyais with ARMADiLLO, an Unmutated Common Ancestor (UCA) is required. The UCA represents the initial V(D)J recombination event that generates the heavy and light chain sequences. ARMADiLLO is agnostic about how the UCA is generated so a variaty of methods can be employed. Additionally, since the method of determining the UCA is unimportant, any precurser sequence can be used.

UCAs can be separately submitted to ARMADiLLO or combined with the sequence files.  ARMADiLLO natively supports the output of:

- `Partis <https://github.com/psathyrella/Partis>`_

- `Cloanalyst <http://www.bu.edu/computationalimmunology/research/software/>`_
  

---------

`Wiehe K., Bradley T., Meyerhoff R.R., Hart C. Williams W.B., Easterhoff D., Faison W.J., Kepler T.B., Saunders K.O., Alam S.M., Bonsignori M. and Haynes B.F. (2018) Functional Relevance of Improbable Antibody Mutations for HIV Broadly Neutralizing Antibody Development. Cell Host & Microbe. 23(6):759-765. <https://pubmed.ncbi.nlm.nih.gov/29861171/>`_
